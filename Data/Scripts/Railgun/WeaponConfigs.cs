﻿using System.Collections.Generic;
using VRageMath;

namespace Whiplash.Railgun
{
    class WeaponConfigs
    {
        public static List<WeaponConfig> FixedGunConfigs = new List<WeaponConfig>()
        {
            // Create config for fixed railgun
            new WeaponConfig()
            {
                ConfigVersionKey = "v1.0.0-mainline",
                BlockSubtype = "LargeRailGunLZM",
                ConfigID = "Railgun",
                ConfigFileName = "RailgunConfig.sbc",

                FireSoundName = "WepShipLargeRailgunShotLZM",
                FireSoundVolumeMultiplier = 1f,

                ShowReloadMessage = true,
                ReloadMessage = "Railgun reloading",

                DrawMuzzleFlash = false,
                MuzzleFlashSpriteName = "Muzzle_Flash_Large",
                MuzzleFlashDuration = 0.1f,
                MuzzleFlashScale = 1f,

                TracerColor = new Vector3(0, 0.5f, 1f),
                TracerScale = 5,
                ShouldDrawProjectileTrails = true,
                ProjectileTrailFadeRatio = 0.03f,

                BulletSpawnForwardOffsetMeters = -3,

                ArtificialGravityMultiplier = 2f,
                NaturalGravityMultiplier = 1f,

                ExplodeOnContact = false,
                ContactExplosionRadius = 0f,
                ContactExplosionDamage = 0f,

                PenetrateOnContact = true,
                PenetrationDamage = 33000,
                PenetrationRange = 100f,

                ExplodePostPenetration = false,
                PenetrationExplosionRadius = 0f,
                PenetrationExplosionDamage = 0f,

                IdlePowerDrawBase = 2f,
                ReloadPowerDraw = 200f,
                MuzzleVelocity = 5000f,
                MaxRange = 5000,
                DeviationAngleDeg = 0.01f,
                RecoilImpulse = 1000000,
                ShieldDamageMultiplier = 5,
                RateOfFireRPM = 5,
                HitImpulse = 1000000,
               
                DrawImpactSprite = true,
                ImpactSpriteName = "Explosion_Warhead_02",
                ImpactSpriteDuration = 6f,
                ImpactSpriteScale = 1f,
            },
        };

        public static List<TurretWeaponConfig> TurretConfigs = new List<TurretWeaponConfig>()
        {
            // Create config for turret railgun
            new TurretWeaponConfig()
            {
                ConfigVersionKey = "v1.0.0-mainline",
                BlockSubtype = "LargeRailgunTurretLZM",
                ConfigID = "Railgun Turret",
                ConfigFileName = "RailgunTurretConfig.sbc",

                FireSoundName = "WepShipLargeRailgunShotLZM",
                FireSoundVolumeMultiplier = 1f,

                ShowReloadMessage = true,
                ReloadMessage = "Railgun reloading",

                DrawMuzzleFlash = false,
                MuzzleFlashSpriteName = "Muzzle_Flash_Large",
                MuzzleFlashDuration = 0.1f,
                MuzzleFlashScale = 1f,

                TracerColor = new Vector3(0, 0.5f, 1f),
                TracerScale = 5,
                ShouldDrawProjectileTrails = true,
                ProjectileTrailFadeRatio = 0.03f,

                BulletSpawnForwardOffsetMeters = -3,

                ArtificialGravityMultiplier = 2f,
                NaturalGravityMultiplier = 1f,

                ExplodeOnContact = false,
                ContactExplosionRadius = 0f,
                ContactExplosionDamage = 0f,

                PenetrateOnContact = true,
                PenetrationDamage = 33000,
                PenetrationRange = 100f,

                ExplodePostPenetration = false,
                PenetrationExplosionRadius = 0f,
                PenetrationExplosionDamage = 0f,

                IdlePowerDrawBase = 2f,
                IdlePowerDrawMax = 20f,
                ReloadPowerDraw = 200f,

                MuzzleVelocity = 5000f,
                MaxRange = 5000,
                DeviationAngleDeg = 0.01f,
                RecoilImpulse = 1000000,
                ShieldDamageMultiplier = 5,
                RateOfFireRPM = 5,
                HitImpulse = 1000000,
                
                DrawImpactSprite = true,
                ImpactSpriteName = "Explosion_Warhead_02",
                ImpactSpriteDuration = 6f,
                ImpactSpriteScale = 1f,
            },
        };
    }
}