import sys
import re
import os

TAB = "    "

FIXED_PATTERN = r"new +WeaponConfig\s*\(\s*\)\s*\{([\s\S]*?)\s*\}"
TURRET_PATTERN = r"new +TurretWeaponConfig\s*\(\s*\)\s*\{([\s\S]*?)\s*\}"

SOURCE_FIXED_PATTERN = r"public class WeaponConfig[\s\S]*?\{([\S\s]*?)\}"
SOURCE_TURRET_PATTERN = r"public class TurretWeaponConfig[\s\S]*?\{([\S\s]*?)\}"

FIELD_PATTERN = r"public\s+[A-z]+\s+(.*);"
SUBTYPE_PATTERN = r'BlockSubtype\s*=\s*\"(.*)\"'
CONFIG_FIELD_PATTERN = r'([A-z0-9_]*)\s*\='

def check_configs(raw_text, cfg_pattern, possible_fields):
    weapon_configs = list(re.finditer(cfg_pattern,raw_text))
    results = {}
    for i in range(len(weapon_configs)):
        existing_fields = []
        config_raw = weapon_configs[i].group(1)

        # Find subtype
        subtype = re.search(SUBTYPE_PATTERN, config_raw).group(1)

        # Extract fields
        lines = [x.strip() for x in config_raw.split('\n')]
        for l in lines:
            match = re.search(CONFIG_FIELD_PATTERN, l)
            if match:
                field_name = match.group(1)
                existing_fields.append(field_name)
        
        missing_fields = [x for x in possible_fields if x not in existing_fields]
        if len(missing_fields) > 0:
            results[subtype] = missing_fields
    return results


if len(sys.argv) < 3:
    print("Not enough arguments")
    print("Args must be in the form:")
    print("python {0} [config_file_location] [config_definition_file]".format(sys.argv[0]))
    sys.exit()

# Get all files to look through
configs_file = sys.argv[1]
configs_source = sys.argv[2]

# Get raw text of configs file
configs_raw_text = ""
with open(configs_file, 'r') as f:
    configs_raw_text = f.read()

# Get raw text of source file
source_raw_text = ""
with open(configs_source, 'r') as f:
    source_raw_text = f.read()

# Get all possible fields
source_fixed_raw = re.search(SOURCE_FIXED_PATTERN, source_raw_text).group(1)
fixed_fields = []
turret_fields = []
for line in source_fixed_raw.split('\n'):
    if line:
        match = re.search(FIELD_PATTERN, line)
        if match:
            fixed_fields.append(match.group(1))
            turret_fields.append(match.group(1))

source_turret_raw = re.search(SOURCE_TURRET_PATTERN, source_raw_text).group(1)
for line in source_turret_raw.split('\n'):
    if line:
        match = re.search(FIELD_PATTERN, line)
        if match:
            turret_fields.append(match.group(1))

# Extract fixed weapons
fixed_results = check_configs(configs_raw_text, FIXED_PATTERN, fixed_fields)

# Extract turreted weapons
turret_results  = check_configs(configs_raw_text, TURRET_PATTERN, turret_fields)

for subtype in fixed_results:
    print('\nConfig for {0} does not set the following fields:'.format(subtype))
    for field in fixed_results[subtype]:
        print('  '+field)

for subtype in turret_results:
    print('\nConfig for {0} does not set the following fields:'.format(subtype))
    for field in turret_results[subtype]:
        print('  '+field)

