﻿using Sandbox.ModAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRage.Game;
using VRage.Game.Components;
using VRage.Utils;

namespace Whiplash.Railgun
{
    [MySessionComponentDescriptor(MyUpdateOrder.NoUpdate, 0)]
    class ClientWeaponAPI : MySessionComponentBase
    {
        const long FIXED_GUN_REGESTRATION_NETID = 1411;
        const long TURRET_REGESTRATION_NETID = 1412;
        const long REGESTRATION_REQUEST_NETID = 1413;
        public bool IsServer;

        public override void LoadData()
        {
            base.LoadData();

            MyAPIGateway.Utilities.RegisterMessageHandler(REGESTRATION_REQUEST_NETID, HandleRegistrationRequest);

            MyLog.Default.WriteLine($"Railgun Weapon Framework Client | INFO | Listeners for {ModContext.ModId} registered.");

            MyLog.Default.WriteLine($"Railgun Weapon Framework Client | INFO | {ModContext.ModId} sent registration info.");
            RegisterWeaponConfigs();

            // TODO: Check for weapon framework mod and whine loudly if it is not there
            //foreach (var mod in MyAPIGateway.Session.Mods)
            //    if (mod.PublishedFileId == 1365616918) ShieldMod = true; //DS - detect shield is installed
        }
        
        
        protected override void UnloadData()
        {
            MyAPIGateway.Utilities.UnregisterMessageHandler(REGESTRATION_REQUEST_NETID, HandleRegistrationRequest);
            base.UnloadData();
        }
        
        void HandleRegistrationRequest(object o)
        {
            MyLog.Default.WriteLine($"Railgun Weapon Framework Client | INFO | {ModContext.ModId} received registration request");
            RegisterWeaponConfigs();
        }

        public void RegisterWeaponConfigs()
        {
            foreach (WeaponConfig config in WeaponConfigs.FixedGunConfigs)
            {
                var s = MyAPIGateway.Utilities.SerializeToBinary(config);
                MyAPIGateway.Utilities.SendModMessage(FIXED_GUN_REGESTRATION_NETID, s);
            }

            foreach (TurretWeaponConfig config in WeaponConfigs.TurretConfigs)
            {
                var s = MyAPIGateway.Utilities.SerializeToBinary(config);
                MyAPIGateway.Utilities.SendModMessage(TURRET_REGESTRATION_NETID, s);
            }
        }
    }
}
